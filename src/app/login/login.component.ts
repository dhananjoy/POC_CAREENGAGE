import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Login } from '../Model/login';
import { Router } from '@angular/router';
import { LoginServiceService } from '../shared/login-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  login: Login = new Login();
  StatusCode: number;
  loginArray: Login[] = new Array<Login>();
  constructor(private _fb: FormBuilder, private router: Router,
    private _login: LoginServiceService) { }

  ngOnInit() {
    this.formInit();
   
  }

  formInit() {
    this.loginForm = this._fb.group({
      username: [''],
      password: ['']
    });
  }

  loginData(data) {
    this.login = this.loginForm.value;
    // this.userData.imei = '0';
    console.log(this.login);
    // this._login.loginData(this.login).subscribe(res => {
    //   console.log(res);
    // });
    this._login.loginData(this.login).subscribe(res => {
      this.login = res;
      console.log(this.login);
      this.StatusCode = res.code;
      if (this.StatusCode === 200) {
        localStorage.setItem('token', res.result.auth_object.result.access_token);
        localStorage.setItem('uId', res.result.UID);
        localStorage.setItem('roleId', res.result.roleId);
        localStorage.setItem('email', res.result.emailId);
        localStorage.setItem('personId', res.result.personId);
        localStorage.setItem('personLName', res.result.personLName);
        localStorage.setItem('personMName', res.result.personMName);
        localStorage.setItem('personPrefixValue', res.result.personPrefixValue);
        localStorage.setItem('roleValue', res.result.roleValue);

        this.router.navigateByUrl('/userlist');

      }else {
        console.log('unauthorised');
      }

      // console.log(res.result.auth_object.result.access_token);
      // else if(this){

      // }
    });
  }

}
