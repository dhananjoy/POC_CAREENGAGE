export class UserList {
    personId: number;
    personFName: string;
    personMName: string;
    personLName: string;
    emailId: string;
    DOB: string;
    contactNumber1: string;
    contactNumber2: string;
    preferedAddressId: string;
    createdByPersonId: number;
    updatedByPersonId: number;
    isActive: boolean;
    uid: string;
    patientId: number;
}
