import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { UserListComponent } from './user-list/user-list.component';
import { AuthgaurdService } from './shared/authgaurd.service';


const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: 'userlist',
        component: UserListComponent,
        canActivate: [AuthgaurdService]
    },
    {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
      },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
