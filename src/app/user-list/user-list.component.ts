import { Component, OnInit } from '@angular/core';
import { LoginServiceService } from '../shared/login-service.service';
import { UserList } from '../Model/userlist';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  data: string;
  userlistArray: UserList[] = new Array<UserList>();

  constructor(private _login: LoginServiceService, private route: Router) {
    
   }

  ngOnInit() {
     this._login.getData1().subscribe(res => {
       this.userlistArray = res.result.result;
      // console.log(JSON.stringify(res))'';
      console.log(JSON.stringify(this.userlistArray));
    });
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('uId');
    localStorage.removeItem('roleId');
    localStorage.removeItem('email');
    localStorage.removeItem('personId');
    localStorage.removeItem('personLName');
    localStorage.removeItem('personMName');
    localStorage.removeItem('personPrefixValue');
    localStorage.removeItem('roleValue');
    this.route.navigateByUrl('/login');
  }
}
