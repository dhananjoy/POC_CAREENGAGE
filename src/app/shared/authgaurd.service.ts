import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthgaurdService implements CanActivate {

  constructor(private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    if (localStorage.getItem('token')) {
      // if (sessionStorage.getItem('ud_key')){
      return true;
    } else {
      this.router.navigate(['/login']);
      // , {queryParams: {returnUrl: state.url}}
      return false;
    }
  }


}
