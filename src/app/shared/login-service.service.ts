import { Injectable, OnInit } from '@angular/core';
import { RequestOptions, Headers, Http, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Login } from '../Model/login';


@Injectable()
export class LoginServiceService implements OnInit {

 

  headers: Headers;
  headers1: Headers;
  options: RequestOptions;
  options1: RequestOptions;
  // baseUrl: String = 'http://18.218.129.93/WebAppAPI';

  constructor(private http: Http) {
    this.headers = new Headers({ 'content-type': 'application/json', 'source': 'provider',
    'Authorization': 'Bearer '+localStorage.getItem('token')});
    this.options = new RequestOptions({ headers: this.headers });
  }

  ngOnInit() {
    this.options = new RequestOptions({ headers: this.headers });
  }

  loginData(login: Login): Observable<any> {
    console.log(JSON.stringify(login));
    return this.http.post('http://18.218.129.93/WebAppAPI/login', JSON.stringify(login), this.options)
      .map(res => res.json());
    }

      getData1(): Observable<any> {
        this.headers1 = new Headers({ 'content-type': 'application/json',
          'Authorization': 'Bearer '+localStorage.getItem('token')});
        this.options1 = new RequestOptions({ headers: this.headers1 });
        return this.http.get('http://18.218.129.93/WebAppAPI/Patients/4', this.options1)
        .map(res => res.json());
      }
}


